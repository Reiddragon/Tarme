# Tarme

![](img/tarme-sample-cfg.png)

Tarme is a status generator for panels using the
[i3bar](https://i3wm.org/docs/i3bar-protocol.html)/[swaybar](https://man.archlinux.org/man/extra/sway/swaybar-protocol.7.en)
protocol, written and configured in [CHICKEN Scheme](https://call-cc.org/).

## What's Missing
- Click Events

## Dependencies
- [CHICKEN Scheme](https://wiki.call-cc.org/man/5/Getting%20started#installing-chicken)
    - build-time dependency for static builds
    - a runtime dependency for dynamic builds or running interpreted
- [Eggs](https://eggs.call-cc.org/)
    - srfi-1
    - srfi-18
    - srfi-99
    - json
    - optimism
    - queues

## Installation
Tarme now ships an Egg definition file. To install Tarme, simply clone this
repo, `cd` into the directory, and run `chicken-install`.

```sh
$ git clone https://codeberg.org/Reiddragon/Tarme
$ cd Tarme
$ chicken-install
```

Note: you may need to use `chicken-install -sudo` instead if your Chicken egg
repository is in a location you don't have write access to.

Alternatively, you may simply run Tarme, compiled or interpreted, directly from
the repository.

Once it is built, point your preferred i3bar-compatible panel to Tarme as the
status command.

For i3bar and swaybar

```
bar {
    # config stuffs

    status_command tarme  # assuming Tarme is in $PATH

    # yet more config stuffs
}
```

By default, Tarme will autodetect if it is ran inside a terminal and display
a rough preview of the status as i3bar/swaybar/etc would display it.

Should you want to force Tarme to instead print the i3bar-protocol json in the
terminal, or print the preview when using it with a panel, you can do so using
the `--mode` flag.

```sh
$ tarme --mode i3bar
```

Currently three modes are supported:

- `i3bar` - prints JSON as required by the i3bar-protocol
- `ansi` - prints a text preview with foreground and background colours
  (default preview mode)
- `plaintext` - like `ansi` but without any colours, for cases when ansi escape
  codes are not available or not handled properly

When using `ansi` or `plaintext` modes, you may also specify a custom separator
using the `--separator` flag. Should none be chosen, Tarme will default to "|".

## Configuration
Tarme will look for a configuration file located at
`$XDG_CONFIG_HOME/tarme/config` or `~/.config/tarme/config`. You may also point
Tarme to a different configuration file using the `-c`/`--config` flag.

```sh
$ tarme --config path/to/config
```

Should you choose to compile the configuration file (using the `-shared` flag
so Tarme can `(load ...)` it), Tarme will prefer loading `config.so` when it is
present.

`config` (no file extension) will also be checked (before the `.so` and `.scm`
variants) due to how Chicken's `load` form works, but it should be avoided as
it creates ambiguity.

## Configuration file format
All `(key . value)` pairs bellow are block proprieties as defined in the
[swaybar-protocol](https://man.archlinux.org/man/extra/sway/swaybar-protocol.7.en),
represented as an association list; Scheme symbols are automatically casted to
strings before sending the blocks to the panel.

The configuration file consists of three parameters Tarme pays attention to,
the first two being optional.

- `header` (optional)
    - defines a custom header for the initial i3bar-protocol setup
    - if no header is defined in the configuration file, Tarme will simply use
      a default one
    - the `(version . N)` key is required by the protocol, consult the protocol
      definition for information on what versions are available and what each
      means
    - redefining it after the initial setup has no effect

```Scheme
(header '((version . 1)
          (click_events . #t)))
```

- `global-defaults` (optional)
    - defines the default keys to be set for all blocks

```Scheme
(global-defaults '((separator . #f)
                   (separator_block_width . 0)))
```

- `status-blocks` (required)
    - defines what blocks Tarme should use
    - takes a list of lists of blocks, where the first one is what Tarme
      displays
    - blocks are defined from left to right

```Scheme
(status-blocks
  ;; The first list in the bigger list is the one displayed
  (list (list (BLOCK ...)
               ...)
        ;; Everything after is shadowed
        (list (BLOCK ...)
              ...)))
```

In practice, the top level list should be treated as a stack rather than
a list. The ability to shadow the status allows for things like menus in the
panel status section.

### Blocks
Each block is defined using the following form

```Scheme
(Block static: STATIC
       update: UPDATE)
```

where

- `static`
    - a list of default block proprieties, just like `global-defaults`, which
      it overrides.
    - defaults to `'()` if left undefined
- `update`
    - a single-argument lambda which must return a list of block proprieties
      (just like the `global-defaults` and `BLOCK`-local `static` proprieties)
        - said argument is the whole block, allowing you to access any of its
          fields
    - proprieties returned by it override both `global-defaults` and the
      block's `STATIC`
    - the list may also be empty
    - defaults to `#f` if left undefined

Block fields may be accessed using `(block-FIELD BLOCK)`.

In addition to `static` and `update`, blocks also have an internally-defined
`queue` field. This field holds a click event queue using the the `queues` egg
which must be imported in the config file for the appropriate procedures

The configuration file should also import any Eggs used by the Blocks.

For examples, see [tarme-config](https://codeberg.org/Reiddragon/tarme-config).

### Themes
This repository also contains a [Pywal template](addons/tarme-pywal.scm), and
the `Base16 Classic Dark` theme in the same format.

## Networked REPL Support
Tarme may optionally be built with support for networked REPLs by passing the
`nrepl` feature to `chicken-install`

```sh
$ chicken-install -D nrepl
```

Please note that the [nrepl](https://wiki.call-cc.org/eggref/5/nrepl) egg must
be installed manually before building Tarme this way.

Once built, you may specify a port for Tarme to listen to for starting
networked REPLs using the `--nrepl` flag.

```sh
$ tarme --nrepl 1234
```

For security reasons, Tarme will only listen for connections from 127.0.0.1
(localhost). This is unlikely to ever change.

Once started, one way to connect to the REPL is using rlwrap and netcat.

```sh
$ rlwrap nc localhost 1234
```

For more information on how to connect, see [nrepl's
documentation](https://wiki.call-cc.org/eggref/5/nrepl#practical-use)

