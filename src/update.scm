;; converts the alists resulting from each block into a vector, as the json
;; egg uses vectors of pairs to represent json objects (the curly braces
;; thingies)
(map (lambda (block)
       (if (block? block)
           ;; Append the global defaults, block-specific default values,
           ;; and dynamic values generated from a lambda
           (delete-duplicates
             (append
               ;; if the update function raises an error, replace it with a
               ;; generic error block stating which block had the error
               ;; (relies on the name value in the block's default section)
               (handle-exceptions
                 exn (let ((blk-name (let ((name (alist-ref 'name (block-static block))))
                                      (if name
                                          (->string name)
                                          "<unnamed>"))))
                       (begin (print-error-message
                                exn (current-error-port)
                                (string-append "Error in block '" blk-name "'"))

                              `((full_text . ,(string-append
                                                "Block \""
                                                blk-name
                                                "\" encountered an error"))
                                (urgent . #t))))
                 ;; Fetch the update function, check if there's any click
                 ;; event in the block's queue, and feed it to the update
                 ;; function if there is, or #f if the queue's empty
                 ;;
                 ;; currently click events are disabled as I have no idea
                 ;; what's broken with the current implementation
                 (let ((update-lambda (block-update block)))
                  (if update-lambda
                      (let ((dynamic (update-lambda block)))
                       (if (alist? dynamic)
                           dynamic
                           '((full_text . "Malformatted 'update' result")
                             (urgent . #t))))
                      '())))
               (block-static block)
               (global-defaults))
             (lambda (x y) (eq? (car x) (car y))))
           (begin (print-notice 'error
                                "Not a block, skipping")
                  '())))
     (car (status-blocks)))

