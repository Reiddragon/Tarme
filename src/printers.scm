;; Printers for each mode
(define print-status
  (case mode
    ((i3bar) (lambda (status)
               (json-write (map list->vector status))
               (display ",")
               (flush-output)))

    ((ansi) (lambda (status)
              (display "\r")
              (flush-output)
              (let loop ((status status))
               (unless (null? status)
                 ;; Check if there's a colour, and print the ansi codes for
                 ;; it if there is
                 (let* ((blk (car status))
                        (fg (alist-ref 'color blk))
                        (bg (alist-ref 'background blk))
                        (text (alist-ref 'full_text blk)))
                   (unless (and text
                                (and (string? text)
                                     (string=? text "")))
                     (when fg (display (string-append
                                         "\x1B[38;2;"
                                         (hex->ansi fg)
                                         "m")))
                     (when bg (display (string-append
                                         "\x1B[48;2;"
                                         (hex->ansi bg)
                                         "m")))
                     (display (alist-ref 'full_text (car status)))
                     (when (or fg bg) (display "\x1B[0m")) ;; Reset attributes if anything was changed
                     (when (alist-ref 'separator blk) (display separator))))

                 (loop (cdr status))))))

    ((plaintext) (lambda (status)
                   (display "\r")
                   (flush-output)
                   (let loop ((status status))
                    (unless (null? status)
                      (let ((text (alist-ref 'full_text (car status))))
                       (unless (and text
                                    (and (string? text)
                                         (string=? text "")))
                         (display text)
                         (when (alist-ref 'separator (car status)) (display separator))))
                      (loop (cdr status))))))))
