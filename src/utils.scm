;; Random util lambdas, not exactly Tarme-specific

;; basic but effective way to check command line arguments
(define (getp key lst)
  (let ((res (member key lst)))
   (if res
       (cadr res)
       #f)))

;; Need an alist predicate to check for malformatted "json" alists
(define (alist? alst)
  (if (list? alst)
      (let loop ((alst alst))
       (if (null? alst)
           #t
           (if (pair? (car alst))
               (loop (cdr alst))
               #f)))
      #f))

;; it converts "#RRGGBB" hex to "R;G;B" decimal as expected by 24bit ANSI commands
(define (hex->ansi col)
  (string-intersperse
    (map (lambda (num)
           (number->string (string->number num 16)))
         (string-chop (substring col 1 7) 2))
    ";"))

;; fancy wrapper function for printing errors
(define (print-notice type msg . rst)
  (with-output-to-port
    (current-error-port)
    (lambda ()
      ;; Error label
      (display (case type
                 ((error) "\x1B[31mERROR\x1B[0m:")
                 ((warning) "\x1B[33mWARNING\x1B[0m:")
                 ((notice) "\x1B[34mNOTICE\x1B[0m:")))
      (let loop ((msg (cons msg rst)))
       (unless (null? msg)
         (display "\t") (display (car msg)) (newline)
         (loop (cdr msg)))))))

(define (make-static-parameter val #!optional guard)
  (let ((value val)
        (grd guard))
   (lambda (#!optional val)
     (if val
         (set! value (if grd (grd val) val))
         value))))

