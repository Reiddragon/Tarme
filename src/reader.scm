 ;; Check if there's input available over stdin, and if there is, attempt to
 ;; read a click event object
 ;;
 ;; Unfortunately, this still doesn't work entirely as intended, thus click
 ;; events remain disabled in the default config
 (when (char-ready?)
   (let* ((event (map (lambda (entry)
                        (cons (string->symbol (car entry))
                              (let ((val (cdr entry)))
                               (if (string? val)
                                   (string->symbol val)
                                   val))))
                      (vector->list (json-read))))
          (block (find (lambda (block)
                         (and (eq? (alist-ref 'name (block-static block))
                                   (alist-ref 'name event))
                              (eq? (alist-ref 'instance (block-static block))
                                   (alist-ref 'instance event))))
                       (car (status-blocks)))))
     (when block
       (queue-add! (block-click-events block) event)))
   (read-char))

