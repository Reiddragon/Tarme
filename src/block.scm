(declare (export Block
                 block?
                 block-static
                 block-update
                 block-click-events))

(define BLOCK
  (make-rtd 'block
            #((immutable static)
              (immutable update)
              (mutable click-events))))
(define (Block #!key
               (static '())
               (update #f))
  (if (alist? static)
      ((rtd-constructor BLOCK) static
                               update
                               (cond-expand
                                 (click-events (make-queue))
                                 (else #f)))
      (begin (print-notice 'error
                           "Malformatted 'static' section of block."
                           "Please make sure it is a valid alist")
             #f)))
(define block? (rtd-predicate BLOCK))
(define block-static (rtd-accessor BLOCK 'static))
(define block-update (rtd-accessor BLOCK 'update))
(define block-click-events (rtd-accessor BLOCK 'click-events))

