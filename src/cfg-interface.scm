(declare (export header
                 global-defaults
                 status-blocks))

;; lambdas exposed to the config file, along with related variables
(define header
  (make-static-parameter
    '((version . 1)
      (click_events . #f))
    (lambda (hdr)
      (if (alist? hdr)
          (begin (cond-expand
                   (click-events)
                   (else (when (alist-ref 'click_events hdr)
                           (print-notice 'error "Tarme was built without click event support"))))
                 hdr)
          (begin (print-notice 'error
                               "Improperly formatted header. Ignoring"
                               "Please Make sure the header is an association list and that it has all the correct properties.")
                 (header))))))

(define global-defaults
  (make-static-parameter
    '() (lambda (default-values)
          (if (alist? default-values)
              default-values
              (begin (print-notice 'error
                                   "Malformatted 'global-defaults'. Ignornig."
                                   "Please make sure it is a valid alist.")
                     (global-defaults))))))

(define status-blocks
  (make-static-parameter
    '((Block static: '((name . "placeholder")
                       (full_text . "thanks for using Tarme"))))))

