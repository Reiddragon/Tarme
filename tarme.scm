#!/bin/env -S csi -script
(import scheme
        (chicken base)
        (chicken condition)
        (chicken gc)
        (chicken io)
        (chicken platform)
        (chicken process-context)
        (chicken port)
        (chicken string)
        (srfi 1)  ; list stuffs
        (srfi 18) ; threading
        (srfi 99) ; record type descriptors
        json
        optimism)

(cond-expand
  (click-events (import queues))
  (else))

(cond-expand
  (nrepl (import nrepl))
  (else))

(declare (safe-globals)
         (strict-types))

;; Parse the cli args with optimism
(define arguments
  (parse-command-line
    '(((-c --config) . config)
      ((-h --help))
      ((-m --mode) . mode)
      (--separator . separator))))

(include-relative "src/utils.scm")

(define-constant help (with-input-from-file "help-file" read-string))
(when (or (alist-ref '--help arguments)
          (alist-ref '-h arguments))
  (write-string (string-translate*
                  help `(("$TARME_BIN$" . ,(program-name))
                         ("$NREPL_SUPPORT$" . ,(cond-expand (nrepl "Built with support")
                                                            (else "Built without support")))
                         ("$RUNTIME_MODE$" . ,(cond-expand
                                                (csi "Interpreted")
                                                (compiling "Compiled")))
                         ("$CHICKEN_VERSION$" . ,(chicken-version)))))
  (exit 0))

(define-constant valid-modes '(i3bar ansi plaintext))
;; Check if there's an explicitly requested mode, or determine one based on
;; whether Tarme is running in a terminal or not
(define mode
  (let ((mode (or (alist-ref '--mode arguments)
                  (alist-ref '-m arguments))))
    (cond
      (mode (cond
              ((member (string->symbol mode) valid-modes) (string->symbol mode))
              (else (print-notice 'error
                                  (string-append "Invalid mode requested '" mode "'")
                                  (apply string-append
                                         (intersperse
                                           (cons "Valid modes:"
                                                 (map symbol->string valid-modes))
                                           " ")))
                    (exit 1))))

      ((terminal-port? (current-output-port)) 'ansi)
      (else 'i3bar))))

;; separator used for 'ansi' and 'plaintext' modes
(define separator
  (or (alist-ref '--separator arguments)
      "|"))

(include-relative "src/printers.scm")
(include-relative "src/block.scm")
(include-relative "src/cfg-interface.scm")

;; Loads the configuration file defining all the blocks
;; There's probably a better way to do the CLI arguments lookup
(let ((cfg-file (or (alist-ref '--config arguments)
                    (alist-ref '-c arguments)
                    (string-append (system-config-directory)
                                   "/tarme/config"))))
  (handle-exceptions
    exn (begin
          (print-notice 'error
                        "Failed to load the config file"
                        (string-append
                          "Please make sure '"
                          cfg-file
                          "' exists and is a valid loadable Chicken Scheme file"))
          (print-error-message exn (current-error-port))
          (exit 1))
    (load cfg-file)))

;; Do init stuffs for the current printer ('i3bar' needs a protocol header,
;; other printers may or may not need different init code; 'ansi' and
;; 'plaintext' for example don't use anything
(case mode
  ((i3bar) (json-write (list->vector (header)))
           ;; stdout must be flushed for i3bar to pick up the updates properly
           (display "\x0A[")
           (flush-output)
           ;; Finally read the initial "{" that Swaybar sends when click events are enabled
           (when (alist-ref 'click_events (header))
             (read-char))))

(let ((nrepl-port (alist-ref '--nrepl arguments)))
  (when nrepl-port
    (cond-expand
      (nrepl (thread-start!
               (lambda ()
                 (nrepl (string->number nrepl-port)))))
      (else (print-notice 'error
                          "Tarme was built without nrepl support")))))


(let loop ((start-time (time->seconds (current-time))))
 (print-status (include-relative "src/update.scm"))

 (cond-expand
   (click-events (include-relative "src/reader.scm"))
   (else))

 ;; Sync clock if it falls behind
 (let ((sysclock (time->seconds (current-time)))
       (time (add1 start-time)))
   (when (< time sysclock)
     (display "Clock fell behind, syncing." (current-error-port))
     (newline (current-error-port))
     (set! time sysclock))
   (gc #f)
   (thread-sleep! (seconds->time time))
   (loop time)))

