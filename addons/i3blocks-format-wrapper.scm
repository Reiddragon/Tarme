(define (i3blocklet #!key (static '()) update)
  (Block static: static
         update: (lambda (click-events)
                   (let ((info (capture ,update)))
                    (if (eq? #!eof info)
                        '((full_text  . "")
                          (short_text . ""))
                        (let ((info (string-split info "\n" #t))
                              (ref (lambda (lst index)
                                     (if (< index (length lst))
                                         (list-ref lst index)
                                         ""))))
                          (map cons
                               '(full_text short_text color background)
                               info)))))))

