# Tarme Addons

This directory includes a few extras you might find useful.

- i3blocks-format-wrapper.scm
    - a basic wrapper to use external blocks using the [i3blocks simplified format]](https://github.com/vivien/i3blocks#format)
    - simply `(include ...)` this in your configuration file and then define blocks using

    ```
    (i3blocklet status
                "/path/to/the/blocks/executable")
    ```

    - I've only tested this wrapper with the [battery2](https://github.com/vivien/i3blocks-contrib/tree/master/battery2) and [mediaplayer](https://github.com/vivien/i3blocks-contrib/tree/master/mediaplayer) blocks from [i3blocks-contrib](https://github.com/vivien/i3blocks-contrib), results may vary with other blocks

- tarme-pywal.scm
    - a template for [Pywal](https://github.com/dylanaraps/pywal/) to generate Tarme themes
    - simply place in `~/.config/wal/templates/` then add `(load "$HOME/.cache/wal/tarme-pywal.scm")` in your configuration file (make sure to replace `$HOME` with your actual home directory or manually pull it from the environment using <a href="https://wiki.call-cc.org/man/5/Module%20(chicken%20process-context)#get-environment-variable">get-environment-variable</a>, as `(load ...)` doesn't parse environment variables in the path it was supplied)

